@extends('layout/template')
@section('content')
<div class="container">
	<div class="row mt-4 mb-4 linha">
		<div class="col-md-6"><h5>{{isset($usuario) ? 'Editar usuário' : 'Cadastro de usuário'}}</h5></div>
	</div>
	<form method="post" action="{{isset($usuario) ? '/usuario/alterar' : '/usuario/cadastrar'}}">
		@csrf
		<input type="hidden" name="id_usuario" value="{{$usuario->id_usuario ?? ''}}">
		<div class="row">
			<div class="col-md-6">
				<label>Nome do usuário</label>
				<input type="text" class="form-control" id="nm_usuario" name="nm_usuario" value="{{$usuario->nm_usuario ?? old('nm_usuario')}}">
			</div>
			<div class="col-md-6">
				<label>Login do usuário</label>
				<input type="text" class="form-control" id="ds_login" name="ds_login" value="{{$usuario->ds_login ?? old('ds_login')}}">
			</div>
		</div>
		<div class="row">
			<div class="d-grid gap-2 d-md-flex justify-content-md-end mt-4">
			  	<button class="btn btn-primary me-md-2" type="submit">{{isset($usuario) ? 'Editar' : 'Cadastrar'}}</button>
			  	<button class="btn btn-secondary" type="button" onclick="document.location='/usuario/lista'">Cancelar</button>
			</div>
		</div>
	</form>

</div>   
    
@endsection
