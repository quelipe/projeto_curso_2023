@extends('layout/template')
@section('content')
<div class="container">
	<div class="row mt-4 mb-4 linha">
		<div class="col-md-6"><h5>Visualizar usuário</h5></div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<label>Nome do usuário: </label><br> {{$usuario->nm_usuario}}
			
		</div>
		<div class="col-md-6">
			<label>Login do usuário: </label><br> {{$usuario->ds_login}}
		</div>
	</div>
	<div class="row">
		<div class="d-grid gap-2 d-md-flex justify-content-md-end mt-4">
		  	<button class="btn btn-secondary" type="button" onclick="document.location='/usuario/lista'">Voltar</button>
		</div>
	</div>

</div>   
    
@endsection
