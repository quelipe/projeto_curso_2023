@extends('layout/template')
@section('content')
<div class="container">
	<div class="row mt-4 mb-4 linha">
		<div class="col-md-12"><h5>Lista de veículos</h5></div>
	</div>
	<div class="row mt-4 mb-4">
		<div class="col-md-12 d-grid justify-content-md-end">
			<button type="button" class="btn btn-success" onclick="document.location='/veiculo/cadastro'">Novo veículo</button>
		</div>
	</div>
	<table id="lista_veiculos" class="table table-striped">
		<thead>
			<tr>
				<th>Código</th>
				<th>Marca</th>
				<th>Modelo</th>
				<th>Placa</th>
				<th width="120" style="text-align: center">Ações</th>
			</tr>
		</thead>
		<tbody>
			@foreach($veiculos as $veiculo) 
			<tr>
				<td>{{$veiculo->id_veiculo}}</td>
				<td>{{$veiculo->modelo->marca->ds_marca}}</td>
				<td>{{$veiculo->modelo->ds_modelo}}</td>
				<td>{{$veiculo->ds_placa}}</td>
				<td align="center">
					<div class="acoes">
						<a href="/veiculo/visualizar/{{$veiculo->id_veiculo}}"><i class="fas fa-search-plus"></i></a>
						<a href="/veiculo/editar/{{$veiculo->id_veiculo}}"><i class="far fa-edit"></i></a>
						<a href="#" onclick="excluir({{$veiculo->id_veiculo}}, 'veiculo')"><i class="far fa-trash-alt"></i></a>
					</div>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>

</div>  
<script>
new DataTable('#lista_veiculos');
</script>    
    
@endsection
