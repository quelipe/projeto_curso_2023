@extends('layout/template')
@section('content')
<div class="container">
	<div class="row mt-4 mb-4 linha">
		<div class="col-md-6"><h5>Visualizar veiculo</h5></div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<label>Marca: </label><br> {{$veiculo->modelo->marca->ds_marca}}
			
		</div>
		<div class="col-md-6">
			<label>Modelo: </label><br> {{$veiculo->modelo->ds_modelo}}
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<label>Cor: </label><br> {{$veiculo->ds_cor}}
			
		</div>
		<div class="col-md-4">
			<label>Ano: </label><br> {{$veiculo->ds_ano}}
		</div>
		<div class="col-md-4">
			<label>Placa: </label><br> {{$veiculo->ds_placa}}
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 mt-4">
			<img src="<?php echo asset('/fotos/'.$veiculo->ds_foto) ?>" width="250">
		</div>
	</div>
	<div class="row">
		<div class="d-grid gap-2 d-md-flex justify-content-md-end mt-4">
		  	<button class="btn btn-secondary" type="button" onclick="document.location='/veiculo/lista'">Voltar</button>
		</div>
	</div>

</div>   
    
@endsection
