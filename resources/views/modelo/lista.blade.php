@extends('layout/template')
@section('content')
<div class="container">
	<div class="row mt-4 mb-4 linha">
		<div class="col-md-12"><h5>Lista de modelos</h5></div>
	</div>
	<div class="row mt-4 mb-4">
		<div class="col-md-12 d-grid justify-content-md-end">
			<button type="button" class="btn btn-success" onclick="document.location='/modelo/cadastro'">Novo modelo</button>
		</div>
	</div>
	<table id="lista_modelos" class="table table-striped">
		<thead>
			<tr>
				<th>Código</th>
				<th>Marca</th>
				<th>Modelo</th>
				<th width="120" style="text-align: center">Ações</th>
			</tr>
		</thead>
		<tbody>
			@foreach($modelos as $modelo) 
			<tr>
				<td>{{$modelo->id_modelo}}</td>
				<td>{{$modelo->marca->ds_marca}}</td>
				<td>{{$modelo->ds_modelo}}</td>
				<td align="center">
					<div class="acoes">
						<a href="/modelo/visualizar/{{$modelo->id_modelo}}"><i class="fas fa-search-plus"></i></a>
						<a href="/modelo/editar/{{$modelo->id_modelo}}"><i class="far fa-edit"></i></a>
						<a href="#" onclick="excluir({{$modelo->id_modelo}}, 'modelo')"><i class="far fa-trash-alt"></i></a>
					</div>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>

</div>  
<script>
new DataTable('#lista_modelo');
</script>    
    
@endsection
