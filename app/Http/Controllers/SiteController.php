<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Veiculo;

class SiteController extends Controller
{
    //
    
	public function getAnuncios() {
		
		$objVeiculo = Veiculo::where('fl_ativo', 1)->orderBy('id_veiculo', 'DESC')->get();
		
		return view("site.index", ['arrObjVeiculo' => $objVeiculo]);
	}
	
	public function getAnuncio($id) {
		
		$objVeiculo = Veiculo::where('id_veiculo', $id)->first();
		
		return view("site.anuncio", ['objVeiculo' => $objVeiculo]);
	}
}
