<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Veiculo;
use App\Models\Marca;
use App\Models\Modelo;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
//use Intervention\Image\ImageManagerStatic as Image;

class VeiculoController extends Controller
{
    //
	private $rules = [
		'id_modelo' => 'required',
		'ds_cor' => 'required|max:30',
		'ds_ano' => 'required|max:4',
		'ds_placa' => 'required|max:7|formato_placa_de_veiculo',
	];
	
	private $messages = [
		'required' => 'Campos obrigatórios não informados.',
		'max'      => 'Limite de caracteres.',
		'formato_placa_de_veiculo' => 'Placa de veículo inválida.'
	];
    
	public function listar() {
		$objVeiculo = Veiculo::where("fl_ativo", 1)->get();
		//dd($objVeiculo[0], $objVeiculo[0]->modelo, $objVeiculo[0]->modelo->marca, $objVeiculo[0]->usuario);
		return view("veiculo.lista", ["veiculos" => $objVeiculo]);
	}
	
	public function cadastrar() {
		$objMarca  = Marca::where("fl_ativo", 1)->get();
		return view("veiculo.cadastro", ['marcas' => $objMarca]);
	}
	
	public function store(Request $request) {
		$validar = Validator::make($request->all(), $this->rules, $this->messages);
		if($validar->fails()) {
			return back()->withErrors($validar->errors())->withInput();
		}
		$dados = $request->all();
		
		if($request->foto) {
			$foto = $request->foto;
			$nome = md5(date("YmdHis").rand()).'.'.strtolower($foto->getClientOriginalExtension());
			
			$dados['ds_foto'] = $nome;
			$size = $foto->getSize();
			if(!$size || $size > 2097152) {
				return back()->withErrors("Tamanho de arquivo não permitido.")->withInput();
			}
			$foto->move(public_path('fotos'), $nome);
		}
		
		$dados['id_usuario_inc'] = Auth::user()->id_usuario;
		
		Veiculo::create($dados);
		
		return redirect(route('veiculo.lista'))->with("mensagem", 'Veículo cadastrado com sucesso!');
	}
	
	public function editar($idVeiculo) {
		$objVeiculo = $this->getUmVeiculo($idVeiculo);
		$objMarca  = Marca::where("fl_ativo", 1)->get();
		
		$objModelo = Modelo::where([["fl_ativo", 1],["id_marca", $objVeiculo->modelo->marca->id_marca]])->get();
		if($objVeiculo) {
			return view("veiculo.cadastro", ["veiculo" => $objVeiculo, "marcas" => $objMarca, "modelos" => $objModelo]);
		}
		return redirect(route("veiculo.lista"))->with("mensagem", "Veículo não encontrado!");
	}
	
	public function update(Request $request) {
		$validar = Validator::make($request->all(), $this->rules, $this->messages);
		if($validar->fails()) {
			return back()->withErrors($validar->errors())->withInput();
		}
		$dados = $request->all();
		if($request->foto) {
			$foto = $request->foto;
			$nome = md5(date("YmdHis").rand()).'.'.strtolower($foto->getClientOriginalExtension());
			
			$dados['ds_foto'] = $nome;
			$size = $foto->getSize();
			if(!$size || $size > 2097152) {
				return back()->withErrors("Tamanho de arquivo não permitido.")->withInput();
			}
			$foto->move(public_path('fotos'), $nome);
		}
		$objVeiculo = $this->getUmVeiculo($dados['id_veiculo']);
		$msg = "O veículo não existe!";
		if($objVeiculo) {
			$objVeiculo->update($dados);
			$msg = "Veículo alterado com sucesso!";
		}
		return redirect(route("veiculo.lista"))->with("mensagem", $msg);
	}
	
	public function view($id) {
		$obj = $this->getUmVeiculo($id);
		if($obj) {
			return view("veiculo.visualizar", ["veiculo" => $obj]);
		}
		return redirect(route("veiculo.lista"))->with("mensagem", "Veículo não encontrado!");
	}
	
	public function delete($id) {
		$obj = $this->getUmVeiculo($id);
		$msg = "O veículo não existe!";
		if($obj) {
			$obj->update([
				'fl_ativo' => 0
			]);
			$msg = "Veículo excluído com sucesso!";
			//@unlink($filename);
		}
		return redirect(route("veiculo.lista"))->with("mensagem", $msg);
	}
	
	private function getUmVeiculo($idVeiculo) {
		$objVeiculo = Veiculo::where("id_veiculo", $idVeiculo)->first();
		return $objVeiculo;
	}
	
}
